.PHONY: help pex wheel test

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  pex           to create the bitbucket.pex file"
	@echo "  wheel         to create a wheel distribution"
	@echo "  test          to run the tests"

pex:
	pex -r requirements.txt --python=python2  -e bitbucket:cli -o bitbucket.pex .

wheel:
	python setup.py bdist_wheel --universal

test:
	python setup.py test
