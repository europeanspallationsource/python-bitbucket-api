bitbucket
=========

Command line application to interact with Bitbucket API.


Usage
-----

::

        $ bitbucket.pex  --help
        Usage: bitbucket.pex [OPTIONS] COMMAND [ARGS]...

          Script to interact with Bitbucket API

        Options:
          -c, --conf TEXT       configuration file [default: "~/.bitbucket.yml"]
          --debug / --no-debug  set log level to debug
          --help                Show this message and exit.

        Commands:
          build_status  Update the build status of a repository
          webhook       Create a webhook for specified repositories

The YAML configuration file should include the following::

        bitbucket:
          owner: bitbucket_owner
          client_id: bitbucket_client_id
          client_secret: bitbucket_client_secret

        webhooks:
          - name: webhook name
            url: https://myproxy.mycompany.com/build
            exclude: []
            args:
              jenkins: https://jenkins.mycompany.com
              job: jenkins_job_name
              token: jenkins_token


Note that `exclude` is optional. If used, it shoud be set to a list of repo slug to exclude (no webhook will be installed).


Installation
------------

`bitbucket` is a Python package that could be installed in a virtual environment.
To make it easier to deploy, it is recommended to create a pex_ file, which is a self-contained executable
Python environment.

You should first install the `pex` utility. See https://pex.readthedocs.io/en/stable/buildingpex.html.
Install `pex` in a virtualenv and create an executable::

    $ pip install pex
    $ pex pex requests -c pex -o ~/bin/pex

To create the `bitbucket.pex` file, run at the root of your clone::

    $ make pex

The created `bitbucket.pex` file should run on any machine with the same OS and Python 2.7 installed.
It's a zip archive that contains all the required dependencies.
Note that PyYAML is not architecture independent. A pex file created on OSX will only run on OSX.
To run on Linux, the pex file shall be created on Linux.


Development
-----------

conda_ is the recommended way to create a separate environment.
It's possible to use virtualenv_ if you prefer.

1. Clone the repository

2. Create and activate a new conda environment::

   $ conda env create -n bitbucket -f environment.yml
   $ source activate bitbucket

3. Create an *editable* install::

   (bitbucket) $ pip install -e .

4. You can now run the `bitbucket` command::

   (bitbucket) $ bitbucket --help

5. To run the tests::

   (bitbucket) $ make test


If you want to run the tests using directly `pytest` you have to install the following extra packages in your environment:

- pytest
- pytest-catchlog
- requests-mock


.. _conda: https://conda.io/docs/using/envs.html
.. _virtualenv: https://virtualenv.pypa.io/en/stable/
.. _pex: https://pex.readthedocs.io/en/stable/index.html
